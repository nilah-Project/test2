import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Meteor } from 'meteor/meteor';
import db from '/imports/shared/db.js';

Template.files.onCreated(function filesOnCreated() {
  this.subscribe('files');
});

Template.tags.onCreated(function tagsOnCreated() {
  this.subscribe('tags');
});

Template.tags.helpers({
  tags: function() {
    return db.Tags.find();
  }
})

Template.files.helpers({
  files: function() {
    return db.Files.find();
  },
});

Template.files.events({
  'click button': function(event, instance) {
    $.fileDownload('/dl?name=${this.path}&id=${Meteor.user()._id}');
  },
});
